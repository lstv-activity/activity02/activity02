<?php
	session_start();
	require("connection.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LSTV Activity 02</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#country').change(function()
			{
			    $('#countryForm').submit();
			});

		});
	</script>
	<style>
		#act{
			text-align: center;
		}

		fieldset{
			margin-top: 15px;
		}

		legend{
			font-size: 30px;
			text-align: center;
		}

		h2, h3{
			margin-left: 10px;
		}

		form{
			margin-left: 20px;
		}

		p, table, h4{
			margin-left: 40px;
		}

		table, th, td{
		  	border: 1px solid black;
		 	border-collapse: collapse;
		 	padding: 5px 10px;
		 	text-align: center;
		}

		span{
			color: red;
			margin-left: 10px;
		}

		select{
			width:100px;
			margin-right: 10px;
		}
		
	</style>
</head>
<body>
	<h1 id="act">Activity 02</h1>

	<fieldset>
		<legend>Looping Reference</legend>
		<h2>1. Check if the input is Palindrome.</h2>
		<form method="POST" action="./process.php">
			<input type="hidden" name="input" value="palindrome">
			<label for="userInput">Input:</label>
			<input type="text" name="userInput" id="userInput">
			<button name="for">Check using for loop</button>
			<button name="while">Check using while loop</button>
			<button name="foreach">Check using foreach loop</button>
		</form>
		<?php 
			if(!empty($_SESSION['msg'])){
				echo "<p>" .$_SESSION['msg']. "</p>";
			}
			unset($_SESSION['msg']);
		?>
	
		<h2>2. Use Loop to display expected output.</h2>
		<form method="POST" action="./process.php">
			<button name="generateLoop">Generate</button>
		</form>
		
		<?php
			if(!empty($_SESSION['loop'])){
				$arr = [1, 2, 3, 4, 5];
				$len = count($arr);
				for ($i=0; $i < $len; $i++) { 
					echo "<p>";
					for ($x=$i; $x < $len; $x++) { 
						echo $arr[$x].', ';
					}
					echo "</p>";
				}
			}
			unset($_SESSION['loop']);
		?>
	</fieldset>

	<fieldset>
		<legend>Condition Reference</legend>
		<h2>1. Check whether the Entered Number is Even or Odd.</h2>
		<form method="POST" action="./process.php">
			<label for="num">Enter a number:</label>
			<input type="number" name="num" id="num">
			<button name="check">Check</button>
		</form>
		<?php
			if(!empty($_SESSION['checked'])){
				echo "<p>" .$_SESSION['checked']. "</p>";
			}
			unset($_SESSION['checked']);
		?>

		<h2>2. Use "SWITCH" to display list of city from selected country.</h2>
		<form method="POST" id="countryForm">
			<label>Pick a Country:</label>
			<select id='country' name="country">
				<?php
					//Select country to be selected in the option. 
					if(isset($_POST['country'])){
						if($_POST['country'] == "australia"){
							echo "<option value='australia' selected>Australia</option>";
							echo "<option value='england'>England</option>";
							echo "<option value='germany'>Germany</option>";
							echo "<option value='philippines'>Philippines</option>";
						}
						else if($_POST['country'] == "england"){
							echo "<option value='australia'>Australia</option>";
							echo "<option value='england' selected>England</option>";
							echo "<option value='germany'>Germany</option>";
							echo "<option value='philippines'>Philippines</option>";
						}
						else if($_POST['country'] == "germany"){
							echo "<option value='australia'>Australia</option>";
							echo "<option value='england'>England</option>";
							echo "<option value='germany' selected>Germany</option>";
							echo "<option value='philippines'>Philippines</option>";
						}
						else if($_POST['country'] == "philippines"){
							echo "<option value='australia'>Australia</option>";
							echo "<option value='england'>England</option>";
							echo "<option value='germany'>Germany</option>";
							echo "<option value='philippines' selected>Philippines</option>";
						}
					}else{
						echo "<option value='australia'>Australia</option>";
						echo "<option value='england'>England</option>";
						echo "<option value='germany'>Germany</option>";
						echo "<option value='philippines'>Philippines</option>";
					}
				?>
			</select>
			<label id="city">City:</label>
			<select id="city" name="city">
				<?php 

					//Switch city from their respective country. 
					switch($_POST['country']){ 
						case "australia": 
							echo "<option value='sydney'>Sydney</option>";
							echo "<option value='melbourne'>Melbourne</option>";
							echo "<option value='brisbane'>Brisbane</option>";
						break;

						case "england": 
							echo "<option value='london'>London</option>";
							echo "<option value='birmingham'>Birmingham</option>";
						break;  

						case "germany": 
							echo "<option value='berlin'>Berlin</option>";
							echo "<option value='birmingham'>Hamburg</option>";
						break;

						case "philippines": 
							echo "<option value='manila'>Manila</option>";
							echo "<option value='quezon'>Quezon</option>";
							echo "<option value='makati'>Makati</option>";
						break;

						default:
						echo "<option value='sydney'>Sydney</option>";
						echo "<option value='melbourne'>Melbourne</option>";
						echo "<option value='brisbane'>Brisbane</option>";
					}
				?>
			</select>
		</form>
	</fieldset>
	
	<fieldset>
		<legend>String Function Reference</legend>
		<h2>1. Replace the special character with a space and convert it to upper case.</h2>
		<?php
			$months = 'January-February\March,April May;June:July]August/September,October|November#December';
			echo "<p>".$months."</p>";
			$res =  preg_replace("/[^a-zA-Z]+/",' ',$months);
		?>
		<h3>A. Uppercase</h3>
		<form method="POST">
			<button name="removeChar">Get Uppercase Months without special character</button>
		</form>
		<p>
			<?php 
				//Print the string without the special characters
				if(isset($_POST['removeChar'])){
					echo strtoupper($res);
				}
			?>	
		</p>

		<h3>B. Get the month name from a string below and fill the table with the expected expected value.</h3>
		<form method="POST" id="month">
			<button name="getMonth">Get Month</button>
		</form>
		<br>
		<table>
			<thead>
				<tr>
					<th>Sort</th>
					<th>Month</th>
				</tr>
			</thead>
			<tbody>
				<?php
					//Get the string without special characters and put it in an array.  

					$strMonths = ucfirst($res);
					$arrMonths = explode(" ", $strMonths);

					// Print the array.
					if(isset($_POST['getMonth'])){
						foreach($arrMonths as $key => $month){
							$sort = $key + 1;
							echo "<tr>";
							echo "<td>$sort</td>";
							echo "<td>$month</td>";
							echo "</tr>";
						}
					}
				?>
			</tbody>
		</table>

		<h2>2. Join the column according to Last Name, First Name Middle Initial</h2>
		<form method="POST">
			<button name="getFullName">Get Full Name</button>
		</form>
		<br>
		<?php
			$_SESSION['details'] = fetch_all("SELECT * FROM person_names");
			//Print the lastname, firstname and middle initial  
			if(isset($_POST['getFullName'])){
				foreach($_SESSION['details'] as $name){
					echo "<p>";
					echo $name['lastname'].", ".$name['firstname']." ";
					if($name['middlename'] == NULL){
						echo substr($name['middlename'], 0, 1);
					}else{
						echo substr($name['middlename'], 0, 1).".";
					}
					echo "</p>";
				}
			}
		?>
	</fieldset>
	
	<fieldset>
		<legend>PHP with MySql</legend>
		<h2>1. Add the list of persons to table below:</h2>
		<form method="POST">
			<button name="getData">Get All Data</button>
		</form>
		<br>
		<table>
			<thead>
				<tr>
					<th>Last Name</th>
					<th>Middle Name</th>
					<th>First Name</th>
					<th>Age</th>
				</tr>
			</thead>
			<tbody>
				<?php
					//If the user press the Get Data button it will show the data of every person in database.  
					if(isset($_POST['getData'])){
						foreach($_SESSION['details'] as $name){
							echo "<tr>";
							echo "<td>".$name['lastname']."</td>";
							echo "<td>".$name['middlename']."</td>";
							echo "<td>".$name['firstname']."</td>";
							echo "<td>".$name['age']."</td>";
							echo "</tr>";
						}
					}
					 
					// If the user press the Refresh it will fet the data from the form then will query to the database then display
					if(isset($_POST['refresh'])){
		
						$firstname = $_POST['firstname'];
							
						// Search Option
						if(isset($_POST['searchOption'])){
							if($_POST['searchOption'] == "left"){
								$search = "$firstname%";
							}else if($_POST['searchOption'] == "anywhere"){
								$search = "%$firstname%";
							}
						}

						// Sorted by
						if(isset($_POST['sort'])){
							if($_POST['sort'] == "firstname"){
								$sort = "firstname";
							}else if($_POST['sort'] == "middlename"){
								$sort = "middlename";
							}else if($_POST['sort'] == "lastname"){
								$sort = "lastname";
							}else if($_POST['sort'] == "age"){
								$sort = "age";
							}
						}

						// Ordered by
						if(isset($_POST['order'])){
							if($_POST['order'] == "ascending"){
								$order = "ASC";
							}else if($_POST['order'] == "descending"){
								$order = "DESC";
							}
						}
						

						if(!empty($search) && !empty($sort) && !empty($order)){

							$query = "SELECT * FROM person_names WHERE firstname LIKE '".$search."'". "ORDER BY $sort $order";
							$result = fetch_all($query);
							$totalAges = 0;
							$totalCount = 0; 
							$lessThanForty = 0;
							$personGreaterThanForty = 0;

							foreach($result as $personData){
								
								echo "<tr>";
								echo "<td>".$personData['lastname']."</td>";
								echo "<td>".$personData['middlename']."</td>";
								echo "<td>".$personData['firstname']."</td>";
								echo "<td>".$personData['age']."</td>";
								echo "</tr>";

								$totalAges += $personData['age'];
								$totalCount++;

								if($personData['age']<40){
									$lessThanForty += $personData['age'];
								}
								if($personData['age']>40){
									$personGreaterThanForty++;
								}					
							}
						}
						else if(!empty($firstname)){
							$fname = "%$firstname%";
							$query = "SELECT * FROM person_names WHERE firstname LIKE '".$fname."'";
							$result = fetch_all($query);

							$totalAges = 0;
							$totalCount = 0; 
							$lessThanForty = 0;
							$personGreaterThanForty = 0;

							foreach($result as $nameOne){
								echo "<tr>";
								echo "<td>".$nameOne['lastname']."</td>";
								echo "<td>".$nameOne['middlename']."</td>";
								echo "<td>".$nameOne['firstname']."</td>";
								echo "<td>".$nameOne['age']."</td>";
								echo "</tr>";

								$totalAges += $nameOne['age'];
								$totalCount++;

								if($nameOne['age']<40){
									$lessThanForty += $nameOne['age'];
								}
								if($nameOne['age']>40){
									$personGreaterThanForty++;
								}			
							}
						}
					}
					
				?>
			</tbody>
		</table>
		<br>
		<form method="POST">
			<!-- Firstname -->
			<label>Firstname:</label>
			<input type="text" name="firstname">
			<br>
			<br>

			<!-- Search Option -->
			<label>Search Option:</label>
			<br>
			<input type="radio" id="left" name="searchOption" value="left">
			<label for="left">Left Most</label>
			<input type="radio" id="anywhere" name="searchOption" value="anywhere">
			<label for="anywhere">Anywhere</label>
			<br>
			<br>

			<!-- Sort By -->
			<label>Sort By:</label>
			<br>
			<input type="radio" id="firstname" name="sort" value="firstname">
			<label for="firstname">First Name</label>
			<input type="radio" id="middlename" name="sort" value="middlename">
			<label for="middlename">Middle Name</label>
			<input type="radio" id="lastname" name="sort" value="lastname">
			<label for="lastname">Last Name</label>
			<input type="radio" id="age" name="sort" value="age">
			<label for="age">Age</label>
			<br>
			<br>

			<!-- Order By -->
			<label>Order By:</label>
			<br>
			<input type="radio" id="ascending" name="order" value="ascending">
			<label for="ascending">Ascending</label>
			<input type="radio" id="descending" name="order" value="descending">
			<label for="descending">Descending</label>
			<br>
			<br>

			<!-- Refresh button -->
			<button name="refresh">Refresh</button>
		</form>

		<h4>Total of all Ages: <span><?php if(isset($totalAges)) echo $totalAges;  ?></span></h4>
		<h4>Total Count of all Ages: <span><?php if(isset($totalCount)) echo $totalCount;  ?></span></h4>
		<h4>Total of Ages less than 40: <span><?php if(isset($lessThanForty)) echo $lessThanForty;  ?></span></h4>
		<h4>Total Count of persons age greater than 40: <span><?php if(isset($personGreaterThanForty)) echo $personGreaterThanForty;  ?></span></h4>
	</fieldset>
	
	
</body>
</html>