<?php
	require("connection.php");

	session_start();

	// Checks whether the word is Palindrome or not using for, while,foreach loop.
	if(isset($_POST['input'])){

		if(isset($_POST['for']) && !empty($_POST['userInput'])){
			// Check if the input is Palindrome using (FOR LOOP)
			$arr = str_split($_POST['userInput']);
			$len = count($arr);
			$checker = $len-1;
			$count = 0;

			for ($i=0; $i < $len ; $i++) { 
				if($arr[$checker] == $arr[$i]){
					$count++;
					$checker--;
				}
			}

			if($count == $len){
				$_SESSION['msg'] = $_POST['userInput']." is a palindrome.";
				header('location: ./index.php');
				die();
			}else{
				$_SESSION['msg'] = $_POST['userInput']." is not a palindrome.";
				header('location: ./index.php');
				die();
			}
		}
		else if(isset($_POST['while']) && !empty($_POST['userInput'])){
			
			$arr = str_split($_POST['userInput']);
			$len = count($arr);
			$revArr = [];
			$checker = $len-1;
			$i=0;

			while($i<$len){
				$revArr[$i] = $arr[$checker];
				$checker--;
				$i++;	
			}

			if($arr == $revArr){
				$_SESSION['msg'] = $_POST['userInput']." is a palindrome.";
				header('location: ./index.php');
				die();
			}else{
				$_SESSION['msg'] = $_POST['userInput']." is not a palindrome.";
				header('location: ./index.php');
				die();
			}
		}
		else if(isset($_POST['foreach']) && !empty($_POST['userInput'])){
			
			$arr = str_split($_POST['userInput']);
			$len = count($arr);
			$revArr = [];
			$checker = $len-1;
			$i=0;

			foreach($arr as $str){
				$revArr[$i] = $arr[$checker];
				$checker--;
				$i++;	
			}

			if($arr == $revArr){
				$_SESSION['msg'] = $_POST['userInput']." is a palindrome.";
				header('location: ./index.php');
				die();
			}else{
				$_SESSION['msg'] = $_POST['userInput']." is not a palindrome.";
				header('location: ./index.php');
				die();
			}
		}
		else{
			$_SESSION['msg'] = "No input!";
			header('location: ./index.php');
			die();
		}
	}

	// Display Loop
	if(isset($_POST['generateLoop'])){
		$_SESSION['loop'] = "generate";
		header('location: ./index.php');
		die();
	}
	
	// Checks whether the number is Even or Odd
	if(isset($_POST['check']) && !empty($_POST['num'])){
		$num = $_POST['num'];

		if($num%2==0){
			$_SESSION['checked'] = "Entered Number is an Even Number";
			header('location: ./index.php');
			die();
		}else{
			$_SESSION['checked'] = "Entered Number is an Odd Number";
			header('location: ./index.php');
			die();
		}
		
	} else{
		$_SESSION['checked'] = "Please input any number.";
			header('location: ./index.php');
			die();
	}




